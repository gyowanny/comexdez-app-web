package br.com.gyotools.comexdez.web.tec;

import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import br.com.gyotools.comexdez.entities.vo.AcordoVO;
import br.com.gyotools.comexdez.web.tec.AcordosViewModelImpl.AcordosGroupModel;

public class AcordosItemRenderer implements ListitemRenderer<Object>  {

	@Override
	public void render(Listitem item, Object obj, int index) throws Exception {
		AcordoVO vo;
		if (obj instanceof AcordosGroupModel.AcordosGroupInfo){
			AcordosGroupModel.AcordosGroupInfo groupInfo = (AcordosGroupModel.AcordosGroupInfo) obj;
			vo = groupInfo.getFirstChild();
			item.setValue(groupInfo);
			item.setDisabled(true);
			Listcell group = new Listcell(vo.getTec());
			group.setClass("list-group-acordos");
			group.setSpan(4);
			item.appendChild(group);
		}else if (obj instanceof AcordoVO){
			vo = (AcordoVO) obj;
			item.appendChild(new Listcell(vo.getPais(), "/resources/img/pais/" + vo.getCodigoPais() + ".gif"));
			item.appendChild(new Listcell(vo.getPref()));
			item.appendChild(new Listcell(vo.getPrefExp()));
			item.appendChild(new Listcell(vo.getNomeAcordo()));
			item.setValue(vo);
		}else {
			item.setVisible(false);
		}
	}

}
