/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.zk.converters;

import br.com.gyotools.comexdez.entities.vo.AtoAplicacaoVO;

public class AtoAplicacaoConverter extends
		AbstractTypedComboboxConverter<AtoAplicacaoVO, Integer> {

	@Override
	public Object getValorString(Integer propriedadeIdentificadora,
			AtoAplicacaoVO value) {
		String label = null;
		if (value.getCodigo().equals(propriedadeIdentificadora)){
			label = value.getDescricao();
		}
		return label;
	}

	@Override
	public Integer getValue(AtoAplicacaoVO value) {
		return value.getCodigo();
	}

}
