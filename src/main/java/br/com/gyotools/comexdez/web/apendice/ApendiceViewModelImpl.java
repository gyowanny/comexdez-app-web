/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.apendice;

import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;

import br.com.gyotools.comexdez.entities.vo.ApendiceVO;
import br.com.gyotools.comexdez.querydata.command.mybatis.apendice.Apendice;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class ApendiceViewModelImpl extends ViewModelBase implements
		ApendiceViewModel {
	private static final long serialVersionUID = -3760705387512686523L;
	
	private Apendice apendice;
	private ApendiceVO apendiceVO;
	
	@Init
	public void init() throws AplicacaoException{
		apendice = (Apendice) Executions.getCurrent().getArg().get("apendice");
		if (apendice != null){
			prepararDados();
		}else{
			throw new AplicacaoException("O codigo do apêndice não foi informado");
		}
	}
	
	protected void prepararDados() throws AplicacaoException{
		try {
			apendiceVO = getFacadeLocator().getApendiceFacade().getApendiceVO(getApendice());
		} catch (FacadeException e) {
			throw new AplicacaoException("Ocorreu um erro ao buscar o apendice para o codigo "+getApendice(), e);
		}
	}

	public ApendiceVO getApendiceVO() {
		return apendiceVO;
	}

	public void setApendiceVO(ApendiceVO apendiceVO) {
		this.apendiceVO = apendiceVO;
	}

	public Apendice getApendice() {
		return apendice;
	}

	public void setApendice(Apendice apendice) {
		this.apendice = apendice;
	}
	
	public String getTitulo(){
		return Labels.getLabel("apendice.titulo") + " " + Labels.getLabel("apendice."+apendice.name(), apendice.getNome());
	}
}
