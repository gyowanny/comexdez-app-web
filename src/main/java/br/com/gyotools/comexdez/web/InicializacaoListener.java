/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.gyotools.comexdez.querydata.QueryDataManager;
import br.com.gyotools.comexdez.querydata.facade.mybatis.MyBatisFacadeLocatorImpl;

public class InicializacaoListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory
			.getLogger(InicializacaoListener.class);

	public void contextInitialized(ServletContextEvent ctx) {
		logger.info("### INICIANDO APLICACAO...");
		try {
			QueryDataManager.setFacadeLocatorImpl(new MyBatisFacadeLocatorImpl());
			logger.info("Verificando conexão com o banco de dados...");
			QueryDataManager.getInstance().getFacadeLocator().getUtilsFacade().pingBancoDeDados();
			logger.info("### APLICACAO INICIADA!");
		} catch (Exception e) {
			logger.error("Erro ao inicializar a aplicacao", e);
		}
	}

	public void contextDestroyed(ServletContextEvent ctx) {
		logger.info("### APLICACAO ENCERRADA");
	}

}
