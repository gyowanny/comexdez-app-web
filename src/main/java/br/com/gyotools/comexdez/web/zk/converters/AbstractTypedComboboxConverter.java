/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.zk.converters;

import java.util.Iterator;
import java.util.List;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

public abstract class AbstractTypedComboboxConverter<T, B> implements
		Converter<Object, B, Component> {

	@SuppressWarnings("unchecked")
	public Object coerceToUi(B beanProp, Component component,
			BindContext ctx) {
		Object label = "";
		if (component instanceof Combobox){
			if (((Combobox) component).getSelectedItem() != null){
				label = ((Combobox) component).getSelectedItem().getLabel();
			}else{
				List<T> tipos = (List<T>) ((Combobox) component).getModel(); 
				for (Iterator<T> i = tipos.iterator(); i.hasNext();){
					T tipo = i.next();
					label = getValorString(beanProp, tipo);
					if (label != null){
						break;
					}
				}
			}
		}
		return label;
	}

	@SuppressWarnings("unchecked")
	public B coerceToBean(Object compAttr, Component component,
			BindContext ctx) {
		B codigo = null;
		if (compAttr != null){
			if (component instanceof Comboitem){
				T vo = ((Comboitem) compAttr).getValue(); 
				if ( vo != null){
					codigo = getValue(vo);
				}	
			}else if (component instanceof Combobox){
				Comboitem selectedItem = ((Combobox) component).getSelectedItem();
				if (selectedItem != null && selectedItem.getValue() != null){
					codigo = getValue((T) selectedItem.getValue());
				}
			}
		}
		return codigo;
	}
	
	public abstract Object getValorString(B propriedadeIdentificadora, T value);
	
	public abstract B getValue(T value);
	
}
