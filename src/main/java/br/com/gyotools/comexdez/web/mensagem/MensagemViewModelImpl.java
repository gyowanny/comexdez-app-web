/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.mensagem;

import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Messagebox;

import br.com.gyotools.comexdez.entities.vo.MensagemVO;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class MensagemViewModelImpl extends ViewModelBase implements
		MensagemViewModel{
	private static final long serialVersionUID = 205251911902192510L;

	private List<MensagemVO> mensagemVOList;
	private Date dataInicial;
	private Date dataFinal;
	private MensagemVO selectedMensagem;
	private int currentIndex = 0;
	
	@Init
	public void init() throws AplicacaoException{
		try {
			mensagemVOList = getFacadeLocator().getMensagemFacade().getUltimasMensagems();
			if (mensagemVOList != null && !mensagemVOList.isEmpty()){
				selectedMensagem = mensagemVOList.get(0); 
			}
		} catch (FacadeException e) {
			throw new AplicacaoException(e);
		}
	}
	
	@Command
	@NotifyChange("mensagemVOList")
	public void pesquisar() throws AplicacaoException {
		if (dataInicial == null){
			Messagebox.show("Informe a data inicial", "Dados Inválidos", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			try {
				mensagemVOList = getFacadeLocator().getMensagemFacade().getMensagensPorData(dataInicial, dataFinal);
			} catch (FacadeException e) {
				throw new AplicacaoException(e);
			}
		}
	}
	
	public List<MensagemVO> getMensagemVOList() {
		return mensagemVOList;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public MensagemVO getSelectedMensagem() {
		return selectedMensagem;
	}

	public void setSelectedMensagem(MensagemVO selectedMensagem) {
		this.selectedMensagem = selectedMensagem;
	}
	
	@Command
	@NotifyChange("selectedMensagem")
	public void nextMessage(){
		if (currentIndex < this.mensagemVOList.size()){
			currentIndex++;
			selectedMensagem = this.mensagemVOList.get(currentIndex);
		}	
	}
	
	@Command
	@NotifyChange("selectedMensagem")
	public void previousMessage(){
		if (currentIndex > 0){
			currentIndex--;
			selectedMensagem = this.mensagemVOList.get(currentIndex);
		}	
	}
}
