/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.index;

import org.zkoss.zk.ui.Component;

import br.com.gyotools.comexdez.web.AplicacaoException;

public interface IndexViewModel {

	public void showApendice(Component parent, String apendice) throws AplicacaoException;	
	
	public String getLoggedUserName() throws AplicacaoException;
}
