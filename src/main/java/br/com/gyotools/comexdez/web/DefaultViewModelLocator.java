/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web;

import br.com.gyotools.comexdez.web.nesh.NeshViewModel;
import br.com.gyotools.comexdez.web.nesh.NeshViewModelImpl;

public class DefaultViewModelLocator implements ViewModelLocator {

	public NeshViewModel getNeshViewModel() {
		return new NeshViewModelImpl();
	}

}
