/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.AcordoVO;
import br.com.gyotools.comexdez.entities.vo.DecretoVO;
import br.com.gyotools.comexdez.entities.vo.TecVO;

public interface AcordosViewModel {
	
	public TecVO getTecVO();
	public void setTecVO(TecVO tecVO);
	public AcordoVO getSelectedAcordoVO();
	public void setSelectedAcordoVO(AcordoVO acordoVO);
	
	public List<DecretoVO> getDecretos();
	public void setSelectedDecreto(DecretoVO decreto);
	public DecretoVO getSelectedDecreto();
	
	public String getDescricaoDecreto();
	public String getTituloNcm();
	public String getDescricaoAcordo();
	public String getTituloAcordoAtos();
	public String getBandeiraPais(Integer codigoPais); 
}
