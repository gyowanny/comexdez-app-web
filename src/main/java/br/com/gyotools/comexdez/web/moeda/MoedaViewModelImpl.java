/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.moeda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

import br.com.gyotools.comexdez.entities.vo.CotacaoFiltroVO;
import br.com.gyotools.comexdez.entities.vo.CotacaoVO;
import br.com.gyotools.comexdez.entities.vo.MoedaVO;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;

/**
 * 
 * @author Gyo
 *
 */
public class MoedaViewModelImpl extends ViewModelBase implements MoedaViewModel {
	private static final long serialVersionUID = -8491049115815744286L;
	
	private List<MoedaVO> moedaVOList;
	private CotacaoFiltroVO cotacaoFiltro;
	private List<CotacaoVO> cotacaoVOList;
	private MoedaVO moedaSelecionada;
	
	@Init
	public void init() throws AplicacaoException{
		try {
			cotacaoFiltro = new CotacaoFiltroVO();
			cotacaoFiltro.setDataInicial(DateUtils.addDays(new Date(), -7));
			cotacaoFiltro.setDataFinal(new Date());
			moedaVOList = getFacadeLocator().getMoedaFacade().getMoedasVOList();
			pesquisar();
		} catch (FacadeException e) {
			throw new AplicacaoException("Ocorreu um erro inesperado ao carregar a lista de moedas",e);
		}
	}
	
	@Command
	@NotifyChange("cotacaoVOList")
	public void pesquisar() throws AplicacaoException{
		try {
			if (moedaSelecionada != null){
				cotacaoFiltro.setCodigoMoeda(Integer.valueOf(moedaSelecionada.getCodigo()));
			}
			cotacaoVOList = getFacadeLocator().getMoedaFacade().getCotacaoVOList(cotacaoFiltro);
		} catch (FacadeException e) {
			throw new AplicacaoException("Ocorreu um erro ao buscar as cotacoes de moeda", e);
		}
	}
	
	@Command
	@NotifyChange("moedaSelecionada")
	public void onMoedaSelecionada() {
	}
	
	public List<MoedaVO> getMoedaVOList() throws AplicacaoException {
		return moedaVOList;
	}

	public List<CotacaoVO> getCotacaoVOList() throws AplicacaoException {
		return cotacaoVOList;
	}

	public List<MoedaVO> getCodigoMoedaList() throws AplicacaoException {
		List<MoedaVO> list = new ArrayList<MoedaVO>();
		list.addAll(moedaVOList);
		Collections.sort(list, new Comparator<MoedaVO>() {
			public int compare(MoedaVO o1, MoedaVO o2) {
				if (!StringUtils.isBlank(o1.getCodigo()) && !StringUtils.isBlank(o2.getCodigo())  
						&& StringUtils.isNumeric(o1.getCodigo()) && StringUtils.isNumeric(o2.getCodigo())){
					return Integer.valueOf(o1.getCodigo()).compareTo(Integer.valueOf(o2.getCodigo()));
				}else{
					return 0;
				}
			}
		});
		return list;
	}

	public List<MoedaVO> getNomeMoedaList() throws AplicacaoException {
		List<MoedaVO> list = new ArrayList<MoedaVO>();
		list.addAll(moedaVOList);
		Collections.sort(list, new Comparator<MoedaVO>() {
			public int compare(MoedaVO o1, MoedaVO o2) {
				if (StringUtils.isBlank(o1.getNome()) || StringUtils.isBlank(o2.getNome())){
					return 0;
				}else{
					return o1.getNome().compareTo(o2.getNome());
				}
			}
		});
		return list;
	}

	public List<MoedaVO> getSwiftMoedaList() throws AplicacaoException {
		List<MoedaVO> list = new ArrayList<MoedaVO>();
		list.addAll(moedaVOList);
		Collections.sort(list, new Comparator<MoedaVO>() {
			public int compare(MoedaVO o1, MoedaVO o2) {
				if (StringUtils.isBlank(o1.getSwift()) || StringUtils.isBlank(o2.getSwift())){
					return 0;
				}else{
					return o1.getSwift().compareTo(o2.getSwift());
				}
			}
		});
		return list;
	}

	public CotacaoFiltroVO getCotacaoFiltro() {
		return cotacaoFiltro;
	}

	public void setCotacaoFiltro(CotacaoFiltroVO cotacaoFiltro) {
		this.cotacaoFiltro = cotacaoFiltro;
	}

	public MoedaVO getMoedaSelecionada() {
		return moedaSelecionada;
	}

	public void setMoedaSelecionada(MoedaVO moedaSelecionada) {
		this.moedaSelecionada = moedaSelecionada;
	}

}
