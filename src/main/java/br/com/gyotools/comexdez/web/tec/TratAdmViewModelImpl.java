/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.List;

import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Execution;

import br.com.gyotools.comexdez.entities.vo.NotaVO;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class TratAdmViewModelImpl extends ViewModelBase implements
		TratAdmViewModel {
	private static final long serialVersionUID = -8225753998901900621L;
	
	private List<NotaVO> tratAdmVOList;
	private NotaVO selectedTratAdmVO;
	private String descricao;

	@SuppressWarnings("unchecked")
	@Init
	public void init(@ContextParam(ContextType.EXECUTION) Execution exec){
		tratAdmVOList = (List<NotaVO>) exec.getArg().get("tratAdmVOList");
		StringBuilder sb = new StringBuilder();
		if (tratAdmVOList != null){
			for (NotaVO nota: tratAdmVOList){
				sb.append(nota.getDescricao()+"\n");
			}
		}
		descricao = sb.toString();
	}
	
	public List<NotaVO> getTratAdmVOList() {
		return tratAdmVOList;
	}

	public NotaVO getSelectedTratAdmVO() {
		return selectedTratAdmVO;
	}

	@NotifyChange("descricao")
	public void setSelectedTratAdmVO(NotaVO selectedTratAdmVO) {
		this.selectedTratAdmVO = selectedTratAdmVO;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getTitulo() {
		return null;
	}
}
