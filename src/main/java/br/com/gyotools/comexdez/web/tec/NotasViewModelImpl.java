/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.Arrays;
import java.util.List;

import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Execution;

import br.com.gyotools.comexdez.entities.vo.NotaVO;
import br.com.gyotools.comexdez.entities.vo.TecVO;
import br.com.gyotools.comexdez.entities.vo.TipoNota;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class NotasViewModelImpl extends ViewModelBase implements NotasViewModel {
	private static final long serialVersionUID = 340950669936734787L;

	private TipoNota tipoNota;
	private TipoNota selectedTipoNota;
	private TecVO tecVO;
	private String ncmFormatado;
	
	@Init
	public void init(@ContextParam(ContextType.EXECUTION) Execution execution){
		tipoNota = (TipoNota) execution.getArg().get("tipoNota");
		selectedTipoNota = tipoNota;
		tecVO = (TecVO) execution.getArg().get("tecVO");
		ncmFormatado = (String) execution.getArg().get("ncm");
	}
	
	public TipoNota getTipoNota(){
		return tipoNota;
	}

	public String getTituloNota() {
		return Labels.getLabel("tec.notas.descricao.titulo")+ " " + selectedTipoNota;
	}

	public List<TipoNota> getTipoNotas() {
		return Arrays.asList(TipoNota.values());
	}

	@NotifyChange({"descricaoNota","tituloNota"})
	public void setSelectedTipoNota(TipoNota tipoNota) {
		selectedTipoNota = tipoNota;
	}

	public TipoNota getSelectedTipoNota() {
		return selectedTipoNota;
	}

	public String getDescricaoNota() {
		String result;
		if (tecVO == null){
			result = "";
		}else{
			NotaVO nota = null;
			if (TipoNota.DUMP.equals(selectedTipoNota)){
				nota = tecVO.getNotaDump();
			}else if (TipoNota.EDIT.equals(selectedTipoNota)){
				nota = tecVO.getNotaPisCofins(); 
			}else if (TipoNota.II.equals(selectedTipoNota)){
				nota = tecVO.getNotaII();
			}else if (TipoNota.IPI.equals(selectedTipoNota)){
				nota = tecVO.getNotaIPI();
			}
			result = nota != null ? nota.getDescricao() : Labels.getLabel("tec.notas.nenhuma")+" "+selectedTipoNota;
		}
		return result;
	}

	public String getTitulo() {
		return Labels.getLabel("tec.notas.titulo") + " " + "N.C.M. "+ncmFormatado;
	}
}
