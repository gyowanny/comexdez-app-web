/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.TipoNota;

public interface NotasViewModel {

	public TipoNota getTipoNota();
	public String getTituloNota();
	public String getTitulo();
	
	public List<TipoNota> getTipoNotas();
	
	public void setSelectedTipoNota(TipoNota tipoNota);
	public TipoNota getSelectedTipoNota();
	public String getDescricaoNota();
	
}
