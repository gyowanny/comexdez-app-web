/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.nesh;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

import br.com.gyotools.comexdez.entities.vo.CapituloVO;
import br.com.gyotools.comexdez.entities.vo.NeshVO;
import br.com.gyotools.comexdez.entities.vo.SecaoVO;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class NeshViewModelImpl extends ViewModelBase implements NeshViewModel {
	private static final long serialVersionUID = 6366251014532388488L;
	
	private List<SecaoVO> secaoVOList;
	private SecaoVO secaoVO;
	private CapituloVO capituloVO;
	private List<CapituloVO> capituloVOList;
	private NeshVO neshVO;
	
	@Init
	public void init() throws AplicacaoException{
		secaoVO = null;
		try {
			secaoVOList = getFacadeLocator().getNeshFacade().getAllSecaoVO();
		} catch (FacadeException e) {
			throw new AplicacaoException("Erro ao carregar as secoes existentes", e);
		}
		//Quando a nesh e chamada por outra tela, como a TEC, poderá receber o vo Nesh.
		//Se nao for nulo setamos todos os valores necessarios
		neshVO = (NeshVO) Executions.getCurrent().getArg().get("neshVO");
		if (neshVO != null){
			
		}
	}
	
	public List<SecaoVO> getSecaoVOList() throws AplicacaoException {
		return secaoVOList;
	}

	@Command
	@NotifyChange("capituloVOList")
	public void onSecaoSelecionada() throws AplicacaoException{
		neshVO = null;
		if (secaoVO == null){
			capituloVOList = null;
		}
		try {
			capituloVOList = getFacadeLocator().getNeshFacade().getCapituloVOBySecao(secaoVO.getCodigo());
		} catch (FacadeException e) {
			throw new AplicacaoException("Erro ao buscar os capitulos da secao codigo ["+secaoVO.getCodigo()+"]", e);
		}
	}
	
	@Command
	@NotifyChange("neshVO")
	public void onCapituloSelecionado() throws AplicacaoException {
		if(capituloVO == null){
			neshVO = null;
		}
		try{
			neshVO = getFacadeLocator().getNeshFacade().getNeshVOByCapitulo(capituloVO.getIdx());
		} catch (FacadeException e) {
			throw new AplicacaoException("Erro ao buscar conteúdo do NESH para secao ["+secaoVO.getCodigo()+"] e capitulo ["+capituloVO.getCapitulo()+"]", e);
		}
	}
	
	public List<CapituloVO> getCapituloVOList() throws AplicacaoException {
		return capituloVOList;
	}

	public NeshVO getNeshVO() throws AplicacaoException {
		return neshVO;
	}

	public SecaoVO getSecaoVO() {
		return secaoVO;
	}

	public void setSecaoVO(SecaoVO secao) {
		secaoVO = secao;
	}

	public CapituloVO getCapituloVO() {
		return capituloVO;
	}

	public void setCapituloVO(CapituloVO capitulo) {
		capituloVO = capitulo;
	}
}
