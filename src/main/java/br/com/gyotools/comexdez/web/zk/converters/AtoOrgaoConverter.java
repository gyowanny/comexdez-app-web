/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.zk.converters;

import br.com.gyotools.comexdez.entities.vo.AtoOrgVO;

public class AtoOrgaoConverter extends AbstractTypedComboboxConverter<AtoOrgVO, Integer> {

	@Override
	public Object getValorString(Integer propriedadeIdentificadora,
			AtoOrgVO value) {
		String label = null;
		if (value.getCodigo().equals(propriedadeIdentificadora)){
			label = value.getDescricao();
		}
		return label;
	}

	@Override
	public Integer getValue(AtoOrgVO value) {
		return value.getCodigo();
	}

}
