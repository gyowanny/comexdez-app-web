/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.security;

public final class PasswordUtils {

	/**
	 * Valida o password informado
	 * @param toEvaluate Password informado
	 * @param password Password correto
	 * @return True se o password esta correto
	 */
	public static boolean passwordMatches(String toEvaluate, String password){
		return crypt(toEvaluate).equals(password);
	}
	
	/**
	 * Encrypts the given password
	 * @param password
	 * @return
	 */
	public static String crypt(String password){
		return MD5Utils.toHash(password);
	}
}
