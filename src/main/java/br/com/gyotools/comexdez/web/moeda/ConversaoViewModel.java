/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.moeda;

import java.math.BigDecimal;
import java.util.Date;

import br.com.gyotools.comexdez.entities.vo.MoedaVO;
import br.com.gyotools.comexdez.web.AplicacaoException;

public interface ConversaoViewModel {

	void setValor(BigDecimal valor);
	BigDecimal getValor();
	void setMoedaOrigem(MoedaVO moedaOrigem);
	MoedaVO getMoedaOrigem();
	void setMoedaDestino(MoedaVO moedaDestino);
	MoedaVO getMoedaDestino();
	BigDecimal getValorConvertido();
	void setDataCotacao(Date dataCotacao);
	Date getDataCotacao();
	
	void calcular() throws AplicacaoException;
	
}
