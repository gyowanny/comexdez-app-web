/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.nesh;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.CapituloVO;
import br.com.gyotools.comexdez.entities.vo.NeshVO;
import br.com.gyotools.comexdez.entities.vo.SecaoVO;
import br.com.gyotools.comexdez.web.AplicacaoException;

public interface NeshViewModel {

	public void init() throws AplicacaoException;
	
	public void onSecaoSelecionada() throws AplicacaoException;
	
	public List<SecaoVO> getSecaoVOList() throws AplicacaoException;
	public List<CapituloVO> getCapituloVOList() throws AplicacaoException;
	public NeshVO getNeshVO() throws AplicacaoException;
	public SecaoVO getSecaoVO();
	public void setSecaoVO(SecaoVO secao);
	public CapituloVO getCapituloVO();
	public void setCapituloVO(CapituloVO capitulo);
	
}
