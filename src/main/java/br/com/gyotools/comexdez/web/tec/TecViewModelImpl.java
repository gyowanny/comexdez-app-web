/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.MaskFormatter;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import br.com.gyotools.comexdez.entities.vo.AtoVO;
import br.com.gyotools.comexdez.entities.vo.CapituloVO;
import br.com.gyotools.comexdez.entities.vo.CotacaoVO;
import br.com.gyotools.comexdez.entities.vo.SecaoVO;
import br.com.gyotools.comexdez.entities.vo.TecVO;
import br.com.gyotools.comexdez.entities.vo.TipoNota;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.querydata.facade.MoedaFacade;
import br.com.gyotools.comexdez.querydata.facade.TecFacade;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;
import br.com.gyotools.comexdez.web.nesh.NeshViewModel;

public class TecViewModelImpl extends ViewModelBase implements TecViewModel {
	private static final long serialVersionUID = 3365393733545517617L;

	public static enum Consolidada {
		ATOS, ACORDOS, NVE, NESH, TRATADM, ICMS, DUMP, PISCOFINS, II, IPI, SH, ST, CONSNCM, HISTNCM
	}
	
	private List<TecVO> tecVOList;
	private TecVO selectedTecVO;
	private String ncm;
	private String descricao;
	private NeshViewModel neshVm;
	private TecVO topLevelNcm;
	private TecFacade tecFacade;
	private Map<Integer, CotacaoVO> cotacoes;
	
	@Init
	public void init() throws AplicacaoException{
		tecFacade = getFacadeLocator().getTecFacade();
		neshVm = getViewModelLocator().getNeshViewModel();
		neshVm.init();
		carregarCotacoesPrincipaisMoedasDoDia();
	}
	
	private void carregarCotacoesPrincipaisMoedasDoDia(){
		MoedaFacade moedaFacade = getFacadeLocator().getMoedaFacade();
		try {
			cotacoes = moedaFacade.toMap(moedaFacade.getPrincipaisMoedasDoDia());
		} catch (FacadeException e) {
			e.printStackTrace();
		}
	}
	
	@Command
	@NotifyChange({"tecVOList", "capitulo"})
	public void pesquisarNcm() throws AplicacaoException {
		if (descricao == null && ncm == null){
			Messagebox.show("Informe ao menos os dois digitos iniciais da NCM ou a descrição", "Atenção", Messagebox.OK, Messagebox.EXCLAMATION);
		}else if ((descricao == null || descricao.isEmpty()) && (ncm.isEmpty() || ncm.length() < 2)){
			Messagebox.show("Informe ao menos os dois digitos iniciais da NCM", "Atenção", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			try {
				String ncmNormalizada = ncm.replaceAll("\\.", "").trim();//replaceAll("\\s[.]","").trim();
				tecVOList = tecFacade.getTecVOList(ncmNormalizada, descricao);
				if (tecVOList == null || tecVOList.isEmpty()){
					Messagebox.show("Nenhuma informação encontrada para o valor "+ncmNormalizada, "Atenção", Messagebox.OK, Messagebox.EXCLAMATION);
				}else{
					topLevelNcm = tecVOList.get(0);
					if (topLevelNcm.getNcm().length() <= 4){
						tecVOList = tecVOList.subList(1, tecVOList.size());
					}
				}
			} catch (FacadeException e) {
				throw new AplicacaoException(e);
			}
		}
	}
	
	@Command
	@NotifyChange({"tecVOList", "capitulo"})
	public void pesquisarCapitulo() throws AplicacaoException {
		 
		if(getCapituloVO() == null){
			Messagebox.show("Selecione o capítulo que deseja procurar", "Atenção", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			try {
				tecVOList = tecFacade.getTecVOList(getCapituloVO().getCapitulo());
			} catch (FacadeException e) {
				throw new AplicacaoException(e);
			}
		}
	}
	
	@Command
	@NotifyChange("capituloVOList")
	public void onSecaoSelecionada() throws AplicacaoException{
		neshVm.onSecaoSelecionada();
	}
	
	@Command
	public void abrirConsolidada(@BindingParam("tipo") String tipo) throws AplicacaoException{
		atualizarDados();
		if (tipo != null && !tipo.isEmpty() && selectedTecVO != null){ 
			Consolidada cons = Consolidada.valueOf(tipo.toUpperCase());
			String uri = null;
			String tecUri = "/tec/";
			Map<String,Object> args = new HashMap<String, Object>();
			args.put("ncm", getNcmFormatado(selectedTecVO.getNcm()));
			switch (cons) {
			case ATOS:
				args.put("AtoVOList", new ListModelList<AtoVO>(selectedTecVO.getAtos()));
				uri = tecUri+"atos.zul";
				break;
			case NESH:
				args.put("neshVO", selectedTecVO.getNesh());
				uri = "/nesh/nesh.zul";
				break;
			case ACORDOS:
				args.put("tecVO", selectedTecVO);
				uri = tecUri+"acordos.zul";
				break;
			case NVE:
				args.put("nveVOList", selectedTecVO.getNves());
				uri = tecUri+"nve.zul";
				break;
			case TRATADM:
				args.put("tratAdmVOList", selectedTecVO.getTratAdms());
				uri = tecUri+"tratAdm.zul";
				break;
			case ICMS:
				args.put("icmsList", selectedTecVO.getIcmList());
				uri = tecUri+"icms.zul";
				break;
			case DUMP:
			case PISCOFINS:
			case II:
			case IPI:
				TipoNota tipoNota = null; 
				if (Consolidada.DUMP.equals(cons)){
					tipoNota = TipoNota.DUMP;
				}else if (Consolidada.PISCOFINS.equals(cons)){
					tipoNota = TipoNota.EDIT;
				}else if (Consolidada.II.equals(cons)){
					tipoNota = TipoNota.II;
				}else if (Consolidada.IPI.equals(cons)){
					tipoNota = TipoNota.IPI;
				}
				args.put("tipoNota", tipoNota);
				args.put("tecVO", selectedTecVO);
				uri = tecUri+"notas.zul";
				break;
			default:
				break;
			}
			if (uri != null){
				Executions.createComponents(uri, null, args);
			}
		}
	}
	
	public List<TecVO> getTecVOList() {
		return tecVOList;
	}
	
	public TecVO getSelectedTecVO() {
		return selectedTecVO;
	}
	
	@NotifyChange({"naoPossuiConsolidada","destaque","tipoDestaque","unidadeMedida","tipoBem"})
	public void setSelectedTecVO(TecVO selected) throws AplicacaoException{
		selectedTecVO = selected;
		contarDadosConsolidados();
	}
	
	private void atualizarDados() throws AplicacaoException{
		if (selectedTecVO == null){
			return;
		}
		try {
			tecFacade.carregarDadosConsolidados(selectedTecVO);
		} catch (FacadeException e) {
			throw new AplicacaoException(e);
		}
	}
	
	private void contarDadosConsolidados() throws AplicacaoException {
		if (selectedTecVO == null){
			return;
		}
		try{
			tecFacade.contarDadosConsolidados(selectedTecVO);
		} catch (FacadeException e) {
			throw new AplicacaoException(e);
		}
	}

	public String getNcm() {
		return ncm;
	}
	public void setNcm(String _ncm) {
		ncm = _ncm;
	}

	private static MaskFormatter FORMATTER = new MaskFormatter();
	public String getNcmFormatado(String ncm) throws AplicacaoException{
		String ncmFormatado;
		if (ncm == null || ncm.isEmpty() || ncm.length() <= 4){
			ncmFormatado = ncm;
		}else{
			FORMATTER.setValueContainsLiteralCharacters(false);
			try {
				if (ncm.length() == 5){
					FORMATTER.setMask("####.#");
				} else if (ncm.length() == 6) {
					FORMATTER.setMask("####.##");
				} else if (ncm.length() == 7) {
					FORMATTER.setMask("####.##.#");
				} else if (ncm.length() == 8) {
					FORMATTER.setMask("####.##.##");
				}
				ncmFormatado = FORMATTER.valueToString(ncm);
			} catch (ParseException e) {
				throw new AplicacaoException(e);
			}
		}
		return ncmFormatado;
	}

	public String getCapitulo() {
		String capitulo;
		if (topLevelNcm == null){
			capitulo = "";
		}else{
			capitulo = topLevelNcm.getRootNcm() + " - " + topLevelNcm.getProduto();
		}	
		return capitulo;
	}
	
	public String getSecao() {
		String secao;
		if (tecVOList == null || tecVOList.isEmpty()){
			secao = "";
		}else{
			TecVO vo = tecVOList.get(0);
			secao = vo.getNcm() + " - " + vo.getProduto();
		}	
		return secao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String desc) {
		descricao = desc;
	}

	public List<SecaoVO> getSecaoVOList() throws AplicacaoException{
		return neshVm.getSecaoVOList();
	}

	public List<CapituloVO> getCapituloVOList() throws AplicacaoException {
		return neshVm.getCapituloVOList();
	}

	public SecaoVO getSecaoVO() {
		return neshVm.getSecaoVO();
	}

	public void setSecaoVO(SecaoVO secao) {
		neshVm.setSecaoVO(secao);
	}

	public CapituloVO getCapituloVO() {
		return neshVm.getCapituloVO();
	}

	public void setCapituloVO(CapituloVO capitulo) {
		neshVm.setCapituloVO(capitulo);
	}
	
	public String getImagemConsolidada(String tipo){
		return naoPossuiConsolidada(tipo) ? "/resources/img/folder-closed_24x24.png" : "/resources/img/folder_24x24.png";
	}
	
	public boolean naoPossuiConsolidada(String tipo){
		boolean result = true;
		if (tipo != null && !tipo.isEmpty() && selectedTecVO != null){ 
			Consolidada cons = Consolidada.valueOf(tipo.toUpperCase());
			switch (cons) {
			case ATOS:
				result = selectedTecVO.getAtosCount() == null || selectedTecVO.getAtosCount() == 0;
				break;
			case NESH:
				result = selectedTecVO.getNeshCount() == null || selectedTecVO.getNeshCount() == 0;
				break;
			case ACORDOS:
				result = selectedTecVO.getAcordosCount() == null || selectedTecVO.getAcordosCount() == 0;
				break;
			case NVE:
				result = selectedTecVO.getNveCount() == null || selectedTecVO.getNveCount() == 0;
				break;
			case TRATADM:
				result= selectedTecVO.getTratAdmCount() == null || selectedTecVO.getTratAdmCount() == 0;
				break;
			case ICMS:
				result = selectedTecVO.getIcmsCount() == null || selectedTecVO.getIcmsCount() == 0;
				break;
			case DUMP:
				result = selectedTecVO.getNotaDumpCount() == null || selectedTecVO.getNotaDumpCount() == 0;
				break;
			case PISCOFINS:
				result = selectedTecVO.getNotaPisCofinsCount() == null || selectedTecVO.getNotaPisCofinsCount() == 0;
				break;
			case II:
				result = selectedTecVO.getNotaIICount() == null || selectedTecVO.getNotaIICount() == 0;
				break;
			case IPI:
				result = selectedTecVO.getNotaIPICount() == null || selectedTecVO.getNotaIPICount() == 0;
				break;	
			default:
				result = true;
				break;
			}
		}
		return result;
	}

	public String getTipoDestaque() {
		return selectedTecVO != null ? selectedTecVO.getTipoDestaque() : "";
	}

	public String getDestaque() {
		return selectedTecVO != null ? selectedTecVO.getDestaque() : "";
	}

	public String getUnidadeMedida() {
		return selectedTecVO != null ? selectedTecVO.getUnidade() : "";
	}

	public String getTipoBem() {
		return "";
	}
	
	public String getTecStyle(String ncm){
		String style;
		if (ncm.trim().length() >= 8){
			style = "";
		}else{
			style = "color: black;font-weight: bold";
		}
		return style;
	}
	
	public boolean isNcmFull(String ncm){
		return ncm.trim().length() >= 8;
	}

	private static final String COTACAO_NAO_ENCONTRADA = "---";
	
	public String getCotacao(Integer codigoMoeda) {
		String cotacao;
		if (!cotacoes.containsKey(codigoMoeda)){
			cotacao = COTACAO_NAO_ENCONTRADA;
		}else{
			cotacao = "R$ " + cotacoes.get(codigoMoeda).getTaxa();
		}
		return cotacao;
	}
}
