/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.NotaVO;

public interface TratAdmViewModel {

	public List<NotaVO> getTratAdmVOList();
	
	public NotaVO getSelectedTratAdmVO();
	public void setSelectedTratAdmVO(NotaVO selectedTratAdmVO);
	public String getTitulo();
	public String getDescricao();
	
}
