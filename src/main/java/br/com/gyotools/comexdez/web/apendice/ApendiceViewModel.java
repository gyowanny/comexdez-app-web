/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.apendice;

import br.com.gyotools.comexdez.entities.vo.ApendiceVO;
import br.com.gyotools.comexdez.querydata.command.mybatis.apendice.Apendice;


public interface ApendiceViewModel {

	public ApendiceVO getApendiceVO();
	public void setApendiceVO(ApendiceVO apendiceVO);
	public Apendice getApendice();
	public void setApendice(Apendice apendice);
	
	public String getTitulo();
	
}
