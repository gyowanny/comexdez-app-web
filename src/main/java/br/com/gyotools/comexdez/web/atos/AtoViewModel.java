/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.atos;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.AtoAplicacaoVO;
import br.com.gyotools.comexdez.entities.vo.AtoFiltroVO;
import br.com.gyotools.comexdez.entities.vo.AtoOrgVO;
import br.com.gyotools.comexdez.entities.vo.AtoResumoIntegraVO;
import br.com.gyotools.comexdez.entities.vo.AtoTipoVO;
import br.com.gyotools.comexdez.entities.vo.AtoVO;
import br.com.gyotools.comexdez.web.AplicacaoException;

public interface AtoViewModel {

	public void pesquisar() throws AplicacaoException;
	public void pesquisarPorConteudo() throws AplicacaoException;
	public void limpar();
	public void abrirResumoIntegra(AtoVO item) throws AplicacaoException;
	
	public AtoFiltroVO getFiltro();
	public List<AtoVO> getAtoVOList();
	public AtoResumoIntegraVO getAtoResumoIntegraVO(AtoVO atoVO);
	
	public List<AtoAplicacaoVO> getAtoAplicacaoVOList();
	public List<AtoOrgVO> getAtoOrgVOList();
	public List<AtoTipoVO> getAtoTipoVOList();
}
