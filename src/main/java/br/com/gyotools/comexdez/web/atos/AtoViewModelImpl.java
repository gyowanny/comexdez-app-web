/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.atos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;

import br.com.gyotools.comexdez.entities.vo.AtoAplicacaoVO;
import br.com.gyotools.comexdez.entities.vo.AtoFiltroVO;
import br.com.gyotools.comexdez.entities.vo.AtoOrgVO;
import br.com.gyotools.comexdez.entities.vo.AtoResumoIntegraVO;
import br.com.gyotools.comexdez.entities.vo.AtoTipoVO;
import br.com.gyotools.comexdez.entities.vo.AtoVO;
import br.com.gyotools.comexdez.querydata.facade.AtoFacade;
import br.com.gyotools.comexdez.querydata.facade.AtoNaoEncontradoException;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;

/**
 * Tela de pesquisa de ATO
 * 
 * @author Gyo
 *
 */
public class AtoViewModelImpl extends ViewModelBase implements AtoViewModel{
	private static final long serialVersionUID = 4741140048072335014L;
	
	private List<AtoVO> atoVOList = new ArrayList<AtoVO>();
	private AtoResumoIntegraVO atoResumoIntegraVO;
	private AtoFiltroVO filtro = new AtoFiltroVO();
	private List<AtoAplicacaoVO> atoAplicacaoVOList;
	private List<AtoOrgVO> atoOrgVOList;
	private List<AtoTipoVO> atoTipoVOList;
	private AtoTipoVO selectedAtoTipo;
	private AtoOrgVO selectedAtoOrgVO;
	private AtoAplicacaoVO selectedAtoAplicacaoVO;
	
	public AtoViewModelImpl() throws Exception{
	}
	
	@Init
	public void init() throws Exception{
		carregarListasAuxiliares();
	}
	
	public List<AtoVO> getAtoVOList() {
		return atoVOList;
	}

	public AtoResumoIntegraVO getAtoResumoIntegraVO(AtoVO atoVO) {
		return atoResumoIntegraVO;
	}

	@Command
	@NotifyChange("atoVOList")
	public void pesquisar() throws AplicacaoException {
		try {
			atoVOList = getFacadeLocator().getAtoFacade().getAtoVOList(getFiltro());
		} catch (AtoNaoEncontradoException e) {
			//Nao precisa lancar excecao, so instanciar uma lista vazia
			atoVOList = new ArrayList<AtoVO>();
		} catch (FacadeException e) {
			throw new AplicacaoException("Ocorreu um problema ao efetuar a pesquisa", e);
		}
	}

	@Command
	@NotifyChange("atoVOList")
	public void pesquisarPorConteudo() throws AplicacaoException {
		
	}
	
	@Command
	@NotifyChange({"filtro", "selectedAtoTipo", "selectedAtoOrgVO", "selectedAtoAplicacaoVO"})
	public void limpar() {
		filtro = new AtoFiltroVO();
		selectedAtoTipo = null;
		selectedAtoOrgVO = null;
		selectedAtoAplicacaoVO = null;
	}

	@Command
	public void abrirResumoIntegra(@BindingParam("item") AtoVO item) throws AplicacaoException{
		if (item.getResumoIntegra() == null){
			try {
				AtoResumoIntegraVO vo = getFacadeLocator().getAtoFacade().getAtoResumoIntegraVO(item.getAtoCodigo());
				item.setResumoIntegra(vo);
			} catch (AtoNaoEncontradoException e) {
			} catch (FacadeException e) {
				throw new AplicacaoException("Ocorreu um erro ao abrir o resumo e íntegra do ato selecionado", e);
			}
		}	
		Map<String,Object> args = new HashMap<String, Object>();
		args.put("atoVO", item);
		Executions.createComponents("/ato/view_resumo_integra.zul", null, args).setVisible(true);
	}
	
	public AtoFiltroVO getFiltro(){
		return filtro;
	}

	public List<AtoAplicacaoVO> getAtoAplicacaoVOList() {
		return atoAplicacaoVOList;
	}

	public List<AtoOrgVO> getAtoOrgVOList() {
		return atoOrgVOList;
	}

	public List<AtoTipoVO> getAtoTipoVOList() {
		return atoTipoVOList;
	}
	
	private void carregarListasAuxiliares() throws Exception{
		AtoFacade atoFacade = getFacadeLocator().getAtoFacade();
		try {
			atoAplicacaoVOList = atoFacade.getAtoAplicacaoList();
			atoOrgVOList = atoFacade.getAtoOrgVOList();
			atoTipoVOList = atoFacade.getAtoTipoVOList();
		} catch (FacadeException e) {
			throw e;
		}
	}

	public AtoTipoVO getSelectedAtoTipo() {
		return selectedAtoTipo;
	}

	public void setSelectedAtoTipo(AtoTipoVO selectedAtoTipo) {
		this.selectedAtoTipo = selectedAtoTipo;
	}

	public AtoOrgVO getSelectedAtoOrgVO() {
		return selectedAtoOrgVO;
	}

	public void setSelectedAtoOrgVO(AtoOrgVO selectedAtoOrgVO) {
		this.selectedAtoOrgVO = selectedAtoOrgVO;
	}

	public AtoAplicacaoVO getSelectedAtoAplicacaoVO() {
		return selectedAtoAplicacaoVO;
	}

	public void setSelectedAtoAplicacaoVO(AtoAplicacaoVO selectedAtoAplicacaoVO) {
		this.selectedAtoAplicacaoVO = selectedAtoAplicacaoVO;
	}

}
