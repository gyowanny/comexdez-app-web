/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.CapituloVO;
import br.com.gyotools.comexdez.entities.vo.SecaoVO;
import br.com.gyotools.comexdez.entities.vo.TecVO;
import br.com.gyotools.comexdez.web.AplicacaoException;

public interface TecViewModel {

	public List<TecVO> getTecVOList();
	public TecVO getSelectedTecVO();
	public void setSelectedTecVO(TecVO _selected) throws AplicacaoException;
	public String getNcm();
	public void setNcm(String _ncm);
	public String getDescricao();
	public void setDescricao(String desc);
	public SecaoVO getSecaoVO();
	public void setSecaoVO(SecaoVO secao);
	public CapituloVO getCapituloVO();
	public void setCapituloVO(CapituloVO capitulo);
	
	public String getNcmFormatado(String ncm) throws AplicacaoException;
	public String getCapitulo();
	public String getSecao();
	
	public List<SecaoVO> getSecaoVOList() throws AplicacaoException;
	public List<CapituloVO> getCapituloVOList() throws AplicacaoException;
	
	public String getTipoDestaque();
	public String getDestaque();
	public String getUnidadeMedida();
	public String getTipoBem();
	
	public String getCotacao(Integer codigoMoeda);
}
