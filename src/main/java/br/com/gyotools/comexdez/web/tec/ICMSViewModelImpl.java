/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;

import br.com.gyotools.comexdez.entities.vo.ApendiceVO;
import br.com.gyotools.comexdez.entities.vo.NotaVO;
import br.com.gyotools.comexdez.entities.vo.UF;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.querydata.facade.TecFacade;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class ICMSViewModelImpl extends ViewModelBase implements ICMSViewModel {
	private static final long serialVersionUID = -6305033820544962783L;

	private static final List<UF> UF_LIST = UF.getValoresOrdenados();

	private UF selectedUF;
	
	private List<NotaVO> icmsList;
	
	private TecFacade tecFacade;
	
	@SuppressWarnings("unchecked")
	@Init
	public void init(@ContextParam(ContextType.EXECUTION) Execution exec){
		icmsList = (List<NotaVO>) exec.getArg().get("icmsList");
		tecFacade = getFacadeLocator().getTecFacade();
	}

	public List<UF> getUfList() {
		return UF_LIST;
	}

	public void setSelectedUF(UF uf) {
		selectedUF = uf;
		showIcmsUF();
	}

	private void showIcmsUF() {
		List<ApendiceVO> icmsUfList;
		try {
			icmsUfList = tecFacade.getICMSByUF(selectedUF);
			StringBuilder descricao = new StringBuilder();
			for (ApendiceVO vo: icmsUfList){
				descricao.append(vo.getApendice()+"\n");
			}
			Map<String,Object> args = new HashMap<String, Object>();
			args.put("descricao", descricao.toString());
			args.put("uf", selectedUF);
			Executions.createComponents("/tec/icmsUf.zul", null, args);
		} catch (FacadeException e) {
			mensagem(null, "Atenção", "Problemas ao carregar a ICMS para "+selectedUF.getNome()+"\n\nEntre em contato com o suporte técnico.");
		}
		
	}
	
	public UF getSelectedUF() {
		return selectedUF;
	}

	public String getDescricaoIcms(){
		StringBuilder sb = new StringBuilder();
		if (icmsList != null){
			for (NotaVO vo: icmsList){
				sb.append(vo.getDescricao()+"\n");
			}
		}
		return sb.toString();
	}
	
}
