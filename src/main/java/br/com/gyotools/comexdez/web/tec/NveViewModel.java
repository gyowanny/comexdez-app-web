/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.NveVO;

public interface NveViewModel {

	public List<NveVO> getNveVOList();
	public String getNveFormatada();
	public String getNcm();
	
}
