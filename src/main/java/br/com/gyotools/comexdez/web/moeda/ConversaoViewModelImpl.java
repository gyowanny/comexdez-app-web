/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.moeda;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Messagebox;

import br.com.gyotools.comexdez.entities.vo.MoedaVO;
import br.com.gyotools.comexdez.querydata.facade.CotacaoNaoEncontradaException;
import br.com.gyotools.comexdez.querydata.facade.DadosInvalidosException;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;

/**
 * 
 * @author Gyo
 *
 */
public class ConversaoViewModelImpl extends ViewModelBase implements ConversaoViewModel {
	private static final long serialVersionUID = -2081659994058923368L;
	
	private static final DecimalFormat FMT = new DecimalFormat("###,###,##0.00000");
	{
		FMT.setMaximumFractionDigits(4);
		FMT.setMinimumFractionDigits(4);
		FMT.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(new Locale("pt", "BR")));
	}
	
	private BigDecimal valor;
	private MoedaVO moedaOrigem;
	private MoedaVO moedaDestino;
	private BigDecimal valorConvertido;
	private List<MoedaVO> moedaVOList;
	private Date dataCotacao = new Date();

	@Init
	public void init() throws AplicacaoException{
		valorConvertido = BigDecimal.ZERO;
		try {
			moedaVOList = getFacadeLocator().getMoedaFacade().getMoedasVOList();
		} catch (FacadeException e) {
			throw new AplicacaoException("Ocorreu um erro inesperado ao carregar a lista de moedas",e);
		}
	}

	@Command
	@NotifyChange("valorConvertidoFormatado")
	public void calcular() throws AplicacaoException{
		try {
			valorConvertido = getFacadeLocator().getMoedaFacade().converter(moedaOrigem, moedaDestino, valor, dataCotacao);
		} catch (FacadeException e) {
			throw new AplicacaoException("Ocorreu um erro ao calcular a conversão", e);
		} catch (CotacaoNaoEncontradaException e) {
			throw new AplicacaoException("Cotação não encontrada", e.getCause());
		} catch (DadosInvalidosException e) {
			Messagebox.show(e.getMessage(), "Dados Inválidos", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}	
	
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setMoedaOrigem(MoedaVO moedaOrigem) {
		this.moedaOrigem = moedaOrigem;
	}

	public MoedaVO getMoedaOrigem() {
		return moedaOrigem;
	}

	public void setMoedaDestino(MoedaVO moedaDestino) {
		this.moedaDestino = moedaDestino;
	}

	public MoedaVO getMoedaDestino() {
		return moedaDestino;
	}

	public String getValorConvertidoFormatado() {
		if (valorConvertido == null) valorConvertido = BigDecimal.ZERO;
		String valorFormatado = FMT.format(valorConvertido);
		return moedaDestino != null && moedaDestino.getSimbolo() != null ? moedaDestino.getSimbolo() + " " + valorFormatado : valorFormatado;
	}

	public List<MoedaVO> getMoedaVOList() {
		return moedaVOList;
	}

	public void setDataCotacao(Date dataCotacao) {
		this.dataCotacao = dataCotacao; 
	}

	public Date getDataCotacao() {
		return dataCotacao;
	}

	public BigDecimal getValorConvertido() {
		return valorConvertido;
	}

}
