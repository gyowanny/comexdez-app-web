/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.index;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;

import br.com.gyotools.comexdez.querydata.command.mybatis.apendice.Apendice;
import br.com.gyotools.comexdez.web.AplicacaoException;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class IndexViewModelImpl extends ViewModelBase implements IndexViewModel{
	private static final long serialVersionUID = -3293252781021673420L;

	private static final String MENSAGEM_ABERTA = "mensagemAberta";
	
	@Init
	public void init() throws Exception {
		Boolean mensagemAberta = (Boolean) Executions.getCurrent().getSession().getAttribute(MENSAGEM_ABERTA);
		if (mensagemAberta == null || !mensagemAberta){
			Executions.getCurrent().getSession().setAttribute(MENSAGEM_ABERTA, Boolean.TRUE);
			Executions.createComponents("/mensagem/mensagem.zul", null, null);
		}	
	}
	
	@Command
	public void showApendice(@BindingParam("parent") Component parent, @BindingParam("apendice") String apendice) throws AplicacaoException {
		Map<String,Object> args = new HashMap<String,Object>();
		args.put("apendice",Apendice.valueOf(apendice));
        Executions.createComponents("/apendice/apendice.zul", parent, args);
	}

	public String getLoggedUserName() throws AplicacaoException {
		Object principal = SecurityUtils.getSubject().getPrincipal();
		String userName;
		if (principal == null){
			userName = "Desconhecido";
		}else{
			userName = principal.toString();
		}
		return userName;
	}
	
}
