/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Execution;

import br.com.gyotools.comexdez.entities.vo.NveVO;
import br.com.gyotools.comexdez.web.ViewModelBase;

public class NveViewModelImpl extends ViewModelBase implements NveViewModel {
	private static final long serialVersionUID = -9147237436321970251L;
	
	private List<NveVO> nveVOList;
	private String ncm;
	
	@SuppressWarnings("unchecked")
	@Init
	public void init(@ContextParam(ContextType.EXECUTION) Execution exec){
		nveVOList = (List<NveVO>) exec.getArg().get("nveVOList");
		ncm = (String) exec.getArg().get("ncm");
	}
	
	public String getNveFormatada() {
		StringBuilder sb = new StringBuilder();
		//Agrupando as NVE por atributo
		if (nveVOList != null && !nveVOList.isEmpty()){
			Map<String, List<NveVO>> map = new HashMap<String, List<NveVO>>();
			for (NveVO vo: nveVOList){
				String key = vo.getAtributo() + " - " + vo.getDescricaoAtributo(); 
				List<NveVO> list = map.get(key);
				if (list == null){
					list = new ArrayList<NveVO>();
					map.put(key, list);
				}
				list.add(vo);
			}
			//Gerando o output
			NveVO vo = nveVOList.get(0);
			sb.append("<h3>N.C.M.: "+ncm+" :: "+vo.getDescricao()+"</h3>");
			for (String key: map.keySet()){
				sb.append("<h3>"+key+"</h3>");
				sb.append("<ul>");
				for (NveVO item: map.get(key)){
					sb.append("<li style='list-style-type: none;'>"+item.getCodEspec()+" - "+item.getEspecificacao()+"</li>");
				}
				sb.append("</ul>");
			}
			
		}
		
		return sb.toString();
	}

	public List<NveVO> getNveVOList(){
		return nveVOList;
	}

	public String getNcm() {
		return ncm;
	}
}
