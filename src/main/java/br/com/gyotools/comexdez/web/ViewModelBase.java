/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;

import br.com.gyotools.comexdez.querydata.QueryDataManager;
import br.com.gyotools.comexdez.querydata.facade.FacadeLocator;

public abstract class ViewModelBase implements Serializable{
	private static final long serialVersionUID = -5895001296582633993L;
	
	private ViewModelLocator viewModelLocator = new DefaultViewModelLocator();
	
	private String email;
	
	public FacadeLocator getFacadeLocator() {
		return QueryDataManager.getInstance().getFacadeLocator();
	}

	public ViewModelLocator getViewModelLocator(){
		return viewModelLocator;
	}
	
	public void mensagem(Component parent, String titulo, String mensagem){
		Map<String,Object> args = new HashMap<String, Object>();
		args.put("titulo", titulo);
		args.put("mensagem", mensagem);
		Executions.createComponents("/comum/mensagem_popup.zul", parent, args);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
