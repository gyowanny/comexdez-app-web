/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.moeda;

import java.util.List;

import br.com.gyotools.comexdez.entities.vo.CotacaoVO;
import br.com.gyotools.comexdez.entities.vo.MoedaVO;
import br.com.gyotools.comexdez.web.AplicacaoException;

public interface MoedaViewModel {

	public List<MoedaVO> getMoedaVOList() throws AplicacaoException;
	public List<CotacaoVO> getCotacaoVOList() throws AplicacaoException;
	
	public List<MoedaVO> getCodigoMoedaList() throws AplicacaoException;
	public List<MoedaVO> getNomeMoedaList() throws AplicacaoException;
	public List<MoedaVO> getSwiftMoedaList() throws AplicacaoException;
	
}
