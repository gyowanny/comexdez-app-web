/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.tec;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zul.GroupComparator;
import org.zkoss.zul.GroupsModelArray;

import br.com.gyotools.comexdez.entities.vo.AcordoVO;
import br.com.gyotools.comexdez.entities.vo.DecretoVO;
import br.com.gyotools.comexdez.entities.vo.TecVO;

public class AcordosViewModelImpl implements AcordosViewModel {

	private TecVO tecVO;
	private AcordoVO selectedAcordoVO;
	private DecretoVO selectedDecreto;

	@Init
	public void init(@ContextParam(ContextType.EXECUTION) Execution execution) {
		tecVO = (TecVO) execution.getArg().get("tecVO");
	}

	public AcordosGroupModel getAcordosModel() {
		return new AcordosGroupModel(tecVO.getAcordos(), new AcordoVOComparator());
	}

	public TecVO getTecVO() {
		return tecVO;
	}

	public void setTecVO(TecVO tecVO) {
		this.tecVO = tecVO;
	}

	public String getTituloNcm() {
		return "N.C.M.: " + tecVO.getNcm() + " (" + tecVO.getSequencial() + ")";
	}

	public AcordoVO getSelectedAcordoVO() {
		return selectedAcordoVO;
	}

	@NotifyChange({ "decretos", "descricaoAcordo", "tituloAcordoAtos", "acordoSelecionado" })
	public void setSelectedAcordoVO(AcordoVO acordoVO) {
		this.selectedAcordoVO = acordoVO;
	}

	public List<DecretoVO> getDecretos() {
		return selectedAcordoVO != null ? selectedAcordoVO.getDecretos() : new ArrayList<DecretoVO>();
	}

	@NotifyChange("descricaoDecreto")
	public void setSelectedDecreto(DecretoVO decreto) {
		selectedDecreto = decreto;
	}

	public DecretoVO getSelectedDecreto() {
		return selectedDecreto;
	}

	public String getDescricaoDecreto() {
		String descricao = "";
		if (selectedDecreto != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("Publicacao D.O.U em " + DecretoVO.getFormattedDou(selectedDecreto));
			sb.append("\n\nSumário: " + selectedDecreto.getComentario());
			sb.append("\n\nConteúdo: " + selectedDecreto.getConteudo());
			sb.append(
					"\n\nNR: A íntegra dos atos legais publicados tem caráter informativo, portanto não substituem os atos legais publicados no Diário Oficial da União (DOU).");
			descricao = sb.toString();
		}
		return descricao;
	}

	public String getDescricaoAcordo() {
		if (selectedAcordoVO != null) {
			if (selectedAcordoVO.getDescricaoCompleta() == null) {
				StringBuilder sb = new StringBuilder();
				sb.append("<h3>Mercadoria: " + tecVO.getProduto() + "</h3>");
				sb.append("<h3>Naladisa/SH: " + tecVO.getNcm() + " (" + tecVO.getSequencial() + ")</h3>");
				sb.append("<div align='center'><h3>Brasil x " + selectedAcordoVO.getPais() + "</h3></div>");
				sb.append("<p><pre>" + selectedAcordoVO.getInstrucao() + " - " + selectedAcordoVO.getDescricao()
						+ "</pre></p>");
				if (selectedAcordoVO.getNotas() != null) {
					sb.append("<br>NOTAS:");
					sb.append("<p><pre>");
					sb.append(selectedAcordoVO.getNotas());
					sb.append("</pre></p>");
				}
				selectedAcordoVO.setDescricaoCompleta(sb.toString());
			}
			return selectedAcordoVO.getDescricaoCompleta();
		} else {
			return "";
		}
	}

	public String getTituloAcordoAtos() {
		return selectedAcordoVO != null ? "Atos legais referentes a acordo entre Brasil X " + selectedAcordoVO.getPais()
				+ " - " + selectedAcordoVO.getInstrucao() : "";
	}

	public boolean isAcordoSelecionado() {
		return selectedAcordoVO != null;
	}

	public String getBandeiraPais(@BindingParam("codigoPais") Integer codigoPais) {
		return "/resources/img/pais/" + codigoPais + ".gif";
	}

	/**
	 * Model para agroupar os acordos por NCM
	 * 
	 * @author gyowannyqueiroz
	 *
	 */
	public static class AcordosGroupModel
			extends GroupsModelArray<AcordoVO, AcordosGroupModel.AcordosGroupInfo, Object, Object> {

		private static final long serialVersionUID = -3371098833907030502L;

		protected AcordosGroupModel(List<AcordoVO> model, Comparator<AcordoVO> comp) {
			super(model.toArray(new AcordoVO[model.size()]), comp);
		}

		protected AcordosGroupInfo createGroupHead(AcordoVO[] groupdata, int index, int col) {
			return new AcordosGroupInfo(groupdata[0], index, col);
		}

		protected Object createGroupFoot(AcordoVO[] groupdata, int index, int col) {
			return groupdata.length;
		}

		public static class AcordosGroupInfo {
			private AcordoVO firstChild;
			private int groupIndex;
			private int colIndex;

			public AcordosGroupInfo(AcordoVO firstChild, int groupIndex, int colIndex) {
				this.firstChild = firstChild;
				this.groupIndex = groupIndex;
				this.colIndex = colIndex;
			}

			public AcordoVO getFirstChild() {
				return firstChild;
			}

			public int getGroupIndex() {
				return groupIndex;
			}

			public int getColIndex() {
				return colIndex;
			}

			@Override
			public String toString() {
				return firstChild.getTec();
			}

		}
	}

	public static class AcordoVOComparator implements Comparator<AcordoVO>, GroupComparator<AcordoVO>, Serializable {

		private static final long serialVersionUID = -4659319307663686100L;

		@Override
		public int compare(AcordoVO o1, AcordoVO o2) {
			return o1.getTec().compareTo(o2.getTec());
		}

		@Override
		public int compareGroup(AcordoVO o1, AcordoVO o2) {
			return compare(o1, o2); 
		}

	}

}
