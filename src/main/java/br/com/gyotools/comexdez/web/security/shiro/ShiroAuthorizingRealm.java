/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.security.shiro;

import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAccount;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import br.com.gyotools.comexdez.entities.vo.UsuarioVO;
import br.com.gyotools.comexdez.querydata.QueryDataManager;
import br.com.gyotools.comexdez.querydata.facade.FacadeException;
import br.com.gyotools.comexdez.querydata.facade.FacadeLocator;
import br.com.gyotools.comexdez.web.security.PasswordUtils;

public class ShiroAuthorizingRealm extends AuthorizingRealm {

	public static final String REALM_NAME = "MY_REALM";

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		String username = (String) principals.getPrimaryPrincipal();
		FacadeLocator locator = QueryDataManager.getInstance().getFacadeLocator();
		Set<String> roles = new HashSet<String>();
		try {
			UsuarioVO usuarioVo = locator.getSecurityFacade().findUsuarioByUserName(username);
			roles.add("ROLE_USER");
		} catch (FacadeException e) {
			throw new AccountException("Problemas ao efetuar login do usuario", e);
		}
		Set<Permission> permissions = new HashSet<Permission>();
		permissions.add(new WildcardPermission("VIEW"));
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo(roles);
		authorizationInfo.setObjectPermissions(permissions);
		
		return authorizationInfo;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		if (!(token instanceof UsernamePasswordToken)) {
			throw new IllegalStateException("Token has to be instance of UsernamePasswordToken class");
		}

		final UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;

		if (userPassToken.getUsername() == null) {
			throw new AccountException("O nome do usuario nao foi informado");
		}

		FacadeLocator locator = QueryDataManager.getInstance().getFacadeLocator();
		SimpleAccount account = null;
		try {
			UsuarioVO usuarioVo = locator.getSecurityFacade().findUsuarioByUserName(userPassToken.getUsername());
			if (!PasswordUtils.passwordMatches(new String(userPassToken.getPassword()), usuarioVo.getSenha())) {
				throw new AccountException("Usuário ou senha inválidos");
			}
			account = new SimpleAccount(usuarioVo.getLogin(), userPassToken.getCredentials(), REALM_NAME);
		} catch (FacadeException e) {
			throw new AccountException("Problemas ao efetuar login do usuario", e);
		}

		return account;
	}

}
