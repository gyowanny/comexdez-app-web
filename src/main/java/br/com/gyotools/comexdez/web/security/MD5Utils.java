/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web.security;

import org.apache.commons.codec.digest.Md5Crypt;

public class MD5Utils {

	private static final String salt = "$1$Legicex$";
	
	public static String toHash(String str) {
		String crypted = Md5Crypt.md5Crypt(str.getBytes(), salt);
		return crypted.substring(salt.length());
	}
	
}
