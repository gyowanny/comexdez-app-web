/**
 * Copyright (R). O código fonte e demais conteúdo existente neste projeto é
 * de propriedade da COMEXDEZ e não pode ser utilizado, reproduzido,
 * modificado sem autorização da mesma.
 */
package br.com.gyotools.comexdez.web;

public class AplicacaoException extends Exception {
	private static final long serialVersionUID = -4143340643143394918L;

	public AplicacaoException() {
		super();
		
	}

	public AplicacaoException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public AplicacaoException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public AplicacaoException(String message) {
		super(message);
		
	}

	public AplicacaoException(Throwable cause) {
		super(cause);
		
	}
}
